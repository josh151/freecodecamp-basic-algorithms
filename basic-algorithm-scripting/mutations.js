/*
Return true
if the string in the first element of the array contains all of the letters of the string in the second element of the array.

For example, ["hello", "Hello"], should
return true because all of the letters in the second string are present in the first, ignoring
case .

The arguments["hello", "hey"] should
return false because the string "hello"
does not contain a "y".

Lastly, ["Alien", "line"], should
return true because all of the letters in "line"
are present in "Alien".
*/
function mutation(arr) {
  var first = arr[0].slice(0).toLowerCase();
  var second = arr[1].slice(0).toLowerCase().split('');
  var arr = second.map(letter => {
    if (first.indexOf(letter) != -1) {
      return true;
    }
    else {
      return false;
    }
  });
  var result = arr.includes(false);
  return !result;
}

console.log(mutation(["hello", "hey"]));
