/*
  One of the simplest and most widely known ciphers
  is a Caesar cipher, also known as a shift cipher. 
  In a shift cipher the meanings of the letters are 
  shifted by some set amount.

  A common modern use is the ROT13 cipher, where the
  values of the letters are shifted by 13 places. 
  Thus 'A' ↔ 'N', 'B' ↔ 'O' and so on.

  Write a function which takes a ROT13 encoded string 
  as input and returns a decoded string.

  All letters will be uppercase. Do not transform any
  non-alphabetic character (i.e. spaces, punctuation),
  but do pass them on.
*/

function rot13(str) { // LBH QVQ VG!
  let splitWord = str.split('');
  let array = splitWord.map((word) => {
    let alphabetLength = 26;
    let alphabetStart = 'A'.charCodeAt(0);
    let normalizeWord = word.charCodeAt(0) - alphabetStart;
    let wordUnicode = (((normalizeWord + 13) % alphabetLength) + alphabetStart);
    let decoded = String.fromCharCode(wordUnicode);
    if (normalizeWord < 0 || normalizeWord > alphabetLength) {
      return word;
    } else {
      return decoded;
    }
  });
  let result = array.join('');
  return result;
}
// Change the inputs below to test
//let output = rot13("SERR PBQR PNZC");
let output = rot13("SERR CVMMN!");
console.log(output);




// let array = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// for (let i = 0; i < array.length; i++) {
//   const element = array[i] ;
//   // console.log(`${element}: ${element.charCodeAt(0)}`);
//   let res = ((((element.charCodeAt(0) - 65) + 13) % 26) + 65);
//   console.log(`${element}: ${res}`);
//}

// console.log(array.length); // 26
// console.log('B'.charCodeAt(0));
// console.log('B'.charCodeAt(0) - 65);
// console.log((('B'.charCodeAt(0) - 65) + 13));
// console.log((('B'.charCodeAt(0) - 65) + 13) % 26);
// console.log(((('B'.charCodeAt(0) - 65) + 13) % 26) + 65);

let res = (((('B'.charCodeAt(0) - 65) + 13) % 26) + 65);
//console.log(res);
let str2 = String.fromCharCode(res);
//console.log(str2);



/*
Unicode values
A: 65
B: 66
C: 67
D: 68
E: 69
F: 70
G: 71
H: 72
I: 73
J: 74
K: 75
L: 76
M: 77
N: 78
O: 79
P: 80
Q: 81
R: 82
S: 83
T: 84
U: 85
V: 86
W: 87
 */