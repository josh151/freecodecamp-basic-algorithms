/* Return the lowest index at which a value(second argument)
   should be inserted into an array(first argument) once it
  has been sorted.The returned value should be a number.

  For example, getIndexToIns([1, 2, 3, 4], 1.5) should 
  return 1 because it is greater than 1(index 0), but less 
  than 2(index 1).

  Likewise, getIndexToIns([20, 3, 5], 19) should
  return 2 because once the array has been sorted it will
  look like[3, 5, 20] and 19 is less than 20(index 2) and 
  greater than 5(index 1).
  */


function getIndexToIns(arr, num) {
  // Find my place in this sorted array.

  arr.sort(function (a, b) {
    if (a > b) {
      return 1;
    } else {
      return -1;
    }
  });
  console.log(arr);
  
  let index = arr.length;
  for (let i = 0; i < arr.length; i++) {
    const element = arr[i];
    console.log(`element ${element}`);
    
    if(num <= element) {
      index = i;
      break;
    }
  }
  return index;
}

// let output = getIndexToIns([60, 40, 30], 45);
//let output = getIndexToIns([5, 3, 20, 3], 5);
let output = getIndexToIns([2, 5, 10], 15);
console.log(output);
